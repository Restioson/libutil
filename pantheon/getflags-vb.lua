-- Pantheon flag parser (rewrite)
local error = printError or error

-- Given a flagstring will return a list of flags.
-- Flagstring:
--  letter(arguments) l(a) ...
-- 	l must be a single character that is not a space, control character, $ or :
--  a must always be a digit or * (all arguments as possible)
--  You can add $ to make it the default flag: 'l(a)$'
--  You can add : to make it depend on other flags: 'c(5):Ab'
-- Flag contents:
--  .flag (String)
--   The letter that the flag represents
--  .argn (Number or String)
--   Number of arguments it requires
--   If set to *, will take as many as it can.
--  .default (Boolean)
--   True if the the flag is marked as default
--  .depends (Boolean)
--   True if the flag has dependencies
--  .depl (Table/Array)
--   Array of flags that the flag depends on
--  .depn (Number)
--   Number of dependencies of the flag
function flags(flagstr)
	if not flagstr then error("flags(): Missing argument.") end
	if type(flagstr) ~= "string" then error("flags(): Argument type is not string.") end
	print("=flags()============")
	print("Given flagstring: "..flagstr)
	local flagl = {}
	local match = {
		flag = "([^%c%s%$:])(%b())",
		argd = "(%*)",
		argn = "(%d+)",
		default = "%b()(%$)",
	}
	for flag in flagstr:gmatch("%S+") do
		print("--------------------")
		print("Processing flag: "..flag)
		local obj = {}
		obj.flag    = ""
		obj.argn    = 0
		obj.default = false
		obj.depends = false
		obj.depl    = {}
		obj.depn    = 0
		-- Flag and argument number
		local fl, argn = flag:match(match.flag)
		local argnd = tonumber(argn:match(match.argn))
		argnd = argnd or argn:match(match.argd)
		if fl and argnd then
			obj.flag = fl
			obj.argn = argnd
		else
			error("flags(): Could not parse flags.")
		end
		print(" Flag: "..fl)
		print(" Arguments: "..argnd)

	-- Defaults and depends
		local isdef = flag:match(match.default)
		if isdef then obj.default = true end
		print(" Default? "..tostring(obj.default))
		flagl[obj.flag] = obj
	end
	print("====================")
	return flagl
end

function getflags(argl, flagl)
	if (not argl) or (not flagl) then error("getflags(): Missing argument.") end
	if (type(argl) ~= "table") or (type(flagl) ~= "table") then error("getflags(): Argument type is not table.") end
	if #argl < 1 then error("getflags(): There is not a flagarg.") end

	local flagarg = argl[1]
	print("=getflags()=========")
	print("Given flagarg: "..flagarg)

	-- Find the default flag
	print("Finding the default flag:")
	local default = {flag = "", obj = {}}
	for flag, obj in pairs(flagl) do
		print(" Processing: "..flag)
		if obj.default then
			print(" Default? Yes")
			default.flag = flag
			default.obj = obj
			break
		end
		print(" Default? No")
	end
	print("Default flag: "..default.flag)

	-- Perform tests to use a pairing type
	local pairingType = 1
	-- Test 1: Check for unexistant flags in flagarg
	for char in flagarg:gmatch("[^%c%s%$:]") do
		print("--------------------")
		print("Performing test 1 on "..char)
		local found = false
		for flag, _ in pairs(flagl) do
			print(" Flag was found. Continuing...")
			if char == flag then found = true end
		end
		if not found then
			pairingType = 2
			print(" Flag was not found. Skipping tests...")
			break
		end
	end
	-- Test 2: Check for number of flags
	if pairingType == 1 then
		local requiredFlags = default.obj.argn
		if requiredFlags ~= "*" then
			for char in flagarg:gmatch("[^%c%s%$:]") do
				print("--------------------")
				print("Performing test 2 on "..char)
				if char ~= default.flag then
					for flag, obj in pairs(flagl) do

						if char == flag then

							if obj.argn == "*" then
								requiredFlags = "*"
							elseif requiredFlags ~= "*" then
								requiredFlags = requiredFlags + obj.argn
							end
							print(" Current required flags: "..tostring(requiredFlags))

						end

					end
				end
			end
		end
		local argn = #argl-1
		if requiredFlags ~= "*" then
			if argn < requiredFlags then
				pairingType = 2
				print(" Not enough flags ("..tostring(requiredFlags).."<"..tostring(argn)..")")
			else print(" Enough flags ("..tostring(requiredFlags)..">"..tostring(argn)..")") end
		else print(" Enough flags (*)") end
	end

	-- Pair each flag by order
	local paired = {}
	if pairingType == 1 then
		local lastIndex = 2
		for char in flagarg:gmatch("[^%c%s%$:]") do for flag, obj in pairs(flagl) do
			if char == flag then
				print("--------------------")
				print("Pairing flag: "..char)
				local values = {}
				local take = obj.argn - 1
				print(" Arguments to take: "..tostring(take))
				if obj.argn == "*" then
					print(" Taking from "..tostring(lastIndex).." to "..tostring(#argl))
					for i=lastIndex, #argl do
						print("  Taking argument: "..tostring(argl[i]))
						table.insert(values, argl[i])
					end
					break
				else
					print(" Taking from "..tostring(lastIndex).." to "..tostring(lastIndex+take))
					for i=lastIndex, lastIndex+take do
						print("  Taking argument: "..tostring(argl[i]))
						table.insert(values, argl[i])
					end
					lastIndex = lastIndex+take+1
				end
				paired[char] = values
			end
		end end
	elseif pairingType == 2 then
		paired[default.flag] = {}
		print("Pairing flag: "..default.flag)
		print(" Arguments to take: "..tostring(default.obj.argn))
		for i=2, default.obj.argn+1 do
			print("  Taking argument: "..argl[i])
			table.insert(paired[default.flag], argl[i])
		end
	end
	print("====================")
	return paired
end

-- Flag iterator
function iargs(paired)
	local k = {}
	local i = 0
	for key, _ in pairs(paired) do table.insert(k, key) end
	local n = #paired
	return function()
		i = i + 1
		if i <= n then return k[i], paired[k[i]] end
	end
end

return flags, getflags, iargs
