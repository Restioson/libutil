-- Pantheon flag parser (rewrite)
local error = printError or error

-- Given a flagstring will return a list of flags.
-- Flagstring:
--  letter(arguments) l(a) ...
-- 	l must be a single character that is not a space, control character, $ or :
--  a must always be a digit or * (all arguments as possible)
--  You can add $ to make it the default flag: 'l(a)$'
-- Flag contents:
--  .flag (String)
--   The letter that the flag represents
--  .argn (Number or String)
--   Number of arguments it requires
--   If set to *, will take as many as it can.
--  .default (Boolean)
--   True if the the flag is marked as default
function flags(flagstr)
	if not flagstr then error("flags(): Missing argument.") end
	if type(flagstr) ~= "string" then error("flags(): Argument type is not string.") end
	local flagl = {}
	local match = {
		flag = "([^%c%s%$:])(%b())",
		argd = "(%*)",
		argn = "(%d+)",
		default = "%b()(%$)",
	}
	for flag in flagstr:gmatch("%S+") do
		local obj = {}

		-- Flag and argument number
		local fl, argn = flag:match(match.flag)
		local argnd = tonumber(argn:match(match.argn))
		argnd = argnd or argn:match(match.argd)
		if fl and argnd then
			obj.flag = fl
			obj.argn = argnd
		else
			error("flags(): Could not parse flags.")
		end

	-- Defaults and depends
		local isdef = flag:match(match.default)
		if isdef then obj.default = true end
		flagl[obj.flag] = obj
	end
	return flagl
end

function getflags(argl, flagl)
	if (not argl) or (not flagl) then error("getflags(): Missing argument.") end
	if (type(argl) ~= "table") or (type(flagl) ~= "table") then error("getflags(): Argument type is not table.") end
	if #argl < 1 then error("getflags(): There is not a flagarg.") end

	local flagarg = argl[1]

	-- Find the default flag
	local default = {flag = "", obj = {}}
	for flag, obj in pairs(flagl) do
		if obj.default then
			default.flag = flag
			default.obj = obj
			break
		end
	end

	-- Perform tests to use a pairing type
	local pairingType = 1
	-- Test 1: Check for unexistant flags in flagarg
	for char in flagarg:gmatch("[^%c%s%$:]") do
		local found = false
		for flag, _ in pairs(flagl) do
			if char == flag then found = true end
		end
		if not found then
			pairingType = 2
			break
		end
	end
	-- Test 2: Check for number of flags
	if pairingType == 1 then
		local requiredFlags = default.obj.argn
		if requiredFlags ~= "*" then
			for char in flagarg:gmatch("[^%c%s%$:]") do
				if char ~= default.flag then
					for flag, obj in pairs(flagl) do
						if char == flag then
							if obj.argn == "*" then
								requiredFlags = "*"
							elseif requiredFlags ~= "*" then
								requiredFlags = requiredFlags + obj.argn
							end
						end
					end
				end
			end
		end
		local argn = #argl - 1
		if requiredFlags ~= "*" then
			if argn < requiredFlags then
				pairingType = 2
			end
		end
	end

	-- Pair each flag by order
	local paired = {}
	if pairingType == 1 then
		local lastIndex = 2
		for char in flagarg:gmatch("[^%c%s%$:]") do for flag, obj in pairs(flagl) do
			if char == flag then
				local values = {}
				local take = obj.argn -1
				if obj.argn == "*" then
					for i=lastIndex, #argl do
						table.insert(values, argl[i])
					end
					break
				else
					for i=lastIndex, lastIndex+take do
						table.insert(values, argl[i])
					end
					lastIndex = lastIndex+take+1
				end
				paired[char] = values
			end
		end end
	elseif pairingType == 2 then
		paired[default.flag] = {}
		for i=2, default.obj.argn+1 do
			table.insert(paired[default.flag], argl[i])
		end
	end
	return paired
end

-- Flag iterator
function iargs(paired)
	local k = {}
	local i = 0
	for key, _ in pairs(paired) do table.insert(k, key) end
	local n = #paired
	return function()
		i = i + 1
		if i <= n then return k[i], paired[k[i]] end
	end
end

return flags, getflags, iargs
