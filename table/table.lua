--[[
libutil : Utils for Pantheon
	      : Pantheon Project
	      :
	      : Filename
	      :  table.lua
	      : Version
	      :  1.0.0-alpha
      	: Author
	      :  daelvn (https://github.com/daelvn)
	      : Documentation
      	:  man libutil/table
	      : Description
	      :  Various utils for tables
]]--

libutil = {tables={}}

-- Packs arguments into a table
-- < ...
-- > table packedData
function libutil.tables.pack(...) return {...} end

-- Unpacks arguments from a table and returns them
-- < table packedData
-- > ...
function libutil.tables.unpack(packedData, i)
	i = i or 1
  if packedData[i] ~= nil then
    return packedData[i], libutil.tables.unpack(packedData, i + 1)
  end
end

-- Iterates a table returning only the value
-- < table list
-- > Iterator
function libutil.tables.list(list)
   local index, count = 0, #collection
   return function ()
      index = index + 1
      if index <= count
      then
         return list[index]
      end
   end
end

return libutil
