-- Include function for Pantheon
local function include(library, file)
	local libraryPath = library:gsub(".", "/")
	local prefixes = {
		"/rom/apis/",
		"/rom/apis/command/",
		"/rom/apis/turtle/",
		"/boot/lib/",
		"/lib/",
		""
	}
	if _ARCH.pantheon then
		local ok, lib
		if file then
			for i, prefix in ipairs(prefixes) do
				ok, lib = loadfile("/bin/lln")("Il", file, prefix..libraryPath, "--")
				if ok then return lib end
			end
		end
		if not file or ok then
			local export
			for i, prefix in ipairs(prefixes) do
				if fs.exists(prefix..libraryPath) then
					export = dofile(prefix..libraryPath)
				elseif fs.exists(prefix..libraryPath..".lua") then
					export = dofile(prefix..libraryPath)
				end
			end
			return export
		end
	else
		local export
		for i, prefix in ipairs(prefixes) do
			if fs.exists(prefix..libraryPath) then
				export = dofile(prefix..libraryPath)
			elseif fs.exists(prefix..libraryPath..".lua") then
				export = dofile(prefix..libraryPath)
			end
		end
		return export
	end
end

return include
